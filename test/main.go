package main

import "bitbucket.org/pureveg/db/wallet"
import "encoding/json"

// import "time"
import "log"

func main() {
	w := wallet.NewWallet("test", "", "/Users/pureveg/workspace/bitcoinBackup/", "user", "pass", "127.0.0.1:8338", 5, 6, 0.000, nil, nil)
	b, _ := json.Marshal(w.GetWallet())
	log.Println(string(b))
	tmp := w.IsValidAddress("asdf")
	if tmp == nil {
		log.Println("invalid")
	} else {
		log.Println(*tmp)
	}
	// i := 0
	// for {
	// 	i++
	// 	s := w.GenNewAddr("")
	// 	if s == nil {
	// 		log.Println("Can not get address")
	// 		time.Sleep(5 * time.Second)
	// 	}
	// 	log.Println(*s)
	// 	time.Sleep(1 * time.Microsecond)
	// }
	/*
		e := new(empty)
		log.Println(e.test())
	*/
}

type empty struct {
}

func (e empty) p(err interface{}) {
	if err != nil {
		panic(err)
	}
}

func (e empty) r(format string) {
	if err := recover(); err != nil {
		log.Printf(format, err)
	}
}

func (e empty) test() string {
	defer e.r("testing %#v")

	e.p("wtf")
	return "fall through"
}

/*
func main() {
}
*/
