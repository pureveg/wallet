package main

import "log"

func recov() {
	if e := recover(); e != nil {
		log.Printf("recover: %q", e)
	}
}

func f() *string {
	defer recov()

	a := make([]string, 10)
	a[10] = "hello"

	s := new(string)
	*s = "hello world"
	return s
}

func main() {
	a := f()
	if a != nil {
		log.Println(*a)
	} else {
		log.Println("get nil pointer")
	}

}
