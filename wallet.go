// This package should be able to help you cope with the bitcoin like clients
// It's simple but useful
// Keypoolrefill and backup when initialize and get the keyPoolSize, set txTimes to 0
// When calling sendto or genNewAddress, txTimes += 1, if txTimes exceeds keyPoolSize, keypoolrefill & backup
//	Functions:
//		Backup
//		Keypoolrefill
//		Validateaddress
//		Sendtoaddress
//		Walletpassphrase
//		Walletlock
//		Listsinceblock
package wallet

import (
	"errors"
	"fmt"
	"github.com/conformal/btcjson"
	"log"
	"sync"
	"time"
)

// implement the wallet structure
type Wallet struct {
	sync.RWMutex
	keyPoolSize int                                      // keypool size
	txTimes     int                                      // transaction times, include new addr and send to addr
	conf        conf                                     // conf
	logFunc     func(format string, args ...interface{}) // error log
	onRecv      func([]Recv) error                       // when deposit is received, do something
}

// wallet conf
type conf struct {
	rpcUser, rpcPass, rpcServer string
	lastBlockHash               string  // last block hash when call listsinceblock
	minConfirmation             int     // minConfirmation when call listsinceblock
	txFee                       float64 // transaction fee
	// should be careful when setting backupDest, otherwise will raise some errors
	backupDest     string // back up destination, should end with /, like /usr/local/ not /usr/local
	detectInterval int    // detect recv interval
	walletPass     string // wallet password
	created        int64  // wallet created timestamp
}

const (
	defaultMinConfirmation = 1 // 1 confirmation
	defaultDetectInterval  = 5 // minute
	defaultBackupDest      = "./"
)

type Recv struct {
	Acct, Txid string
	Amount     float64
}

func NewWallet(rpcUser, rpcPass, rpcServer string, minConfirmation int, backupDest string, detectInterval int, walletPass, lastBlockHash string, txFee float64, created int64, logFunc func(format string, args ...interface{}), onRecv func([]Recv) error) *Wallet {
	w := &Wallet{
		txTimes: 0,
		conf: conf{
			rpcUser:         rpcUser,
			rpcPass:         rpcPass,
			rpcServer:       rpcServer,
			minConfirmation: minConfirmation,
			backupDest:      backupDest,
			detectInterval:  detectInterval,
			walletPass:      walletPass,
			txFee:           txFee,
			lastBlockHash:   lastBlockHash,
			created:         created,
		},
		logFunc: logFunc,
		onRecv:  onRecv,
	}
	if logFunc == nil {
		w.logFunc = log.Printf
	}
	if onRecv == nil {
		w.onRecv = func(s []Recv) error {
			for _, v := range s {
				log.Printf("Receive deposit, account: %s, amount: %f, txid: %s", v.Acct, v.Amount, v.Txid)
			}
			return nil
		}
	}
	if backupDest == "" {
		w.conf.backupDest = defaultBackupDest
	}
	if detectInterval <= 0 {
		w.conf.detectInterval = defaultDetectInterval
	}
	if minConfirmation <= 0 {
		w.conf.minConfirmation = defaultMinConfirmation
	}
	if w.Test() != nil {
		return nil
	}
	return w.init()
}

// Test can be used to check if the setting of wallet is correct
func (w *Wallet) Test() error {
	// test backupDest
	r := []rune(w.getBackupDest())
	if r[len(r)-1] != '/' {
		return errors.New("备份地址不是以'/'结束的")
	}

	// test walletPassPhrase
	if w.walletPassPhrase(1) {
		return errors.New("无法解密钱包, 可能钱包没有加密, 或者是密码错误")
	}
	w.walletLock()

	w.initKeypoolSize()
	//  test backup
	if w.keyPoolRefillAndBackup() {
		return errors.New("无法keypoolrefill 和 备份, 请检查备份路径权限是否足够")
	}

	return nil
}

// init
func (w *Wallet) init() *Wallet {
	w.recvCoin()
	return w
}

// set key pool size
func (w *Wallet) initKeypoolSize() {
	defer rec("Keypoolsize error: %q", w.logFunc)

	msg, err := btcjson.CreateMessage("getinfo")
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	if reply.Error != nil {
		pan(reply.Error.Error())
	}

	w.setKeypoolsize(reply.Result.(btcjson.InfoResult).KeypoolSize - 1)
}

// set, get txFee
func (w *Wallet) SetTxFee(val float64) {
	w.Lock()
	defer w.Unlock()
	w.conf.txFee = val
}
func (w *Wallet) getTxFee() float64 {
	w.RLock()
	defer w.RUnlock()
	return w.conf.txFee
}

// set, get backup destination
func (w *Wallet) SetBackupDest(val string) {
	w.Lock()
	defer w.Unlock()
	w.conf.backupDest = val
}
func (w *Wallet) getBackupDest() string {
	w.RLock()
	defer w.RUnlock()
	return w.conf.backupDest
}

// set, get detect interval
func (w *Wallet) SetDetectInterval(val int) {
	w.Lock()
	defer w.Unlock()
	w.conf.detectInterval = val
}
func (w *Wallet) getDetectInterval() int {
	w.RLock()
	defer w.RUnlock()
	return w.conf.detectInterval
}

// set, get password
func (w *Wallet) SetPassword(val string) {
	w.Lock()
	defer w.Unlock()
	w.conf.walletPass = val
}
func (w *Wallet) getPassword() string {
	w.RLock()
	defer w.RUnlock()
	return w.conf.walletPass
}

// set, get minConfirmation
func (w *Wallet) SetMinConf(val int) {
	w.Lock()
	defer w.Unlock()
	w.conf.minConfirmation = val
}
func (w *Wallet) getMinConf() int {
	w.RLock()
	defer w.RUnlock()
	return w.conf.minConfirmation
}

// set, get lastBlockHash
func (w *Wallet) SetLastBlock(val string) {
	w.Lock()
	defer w.Unlock()
	w.conf.lastBlockHash = val
}
func (w *Wallet) getLastBlock() string {
	w.RLock()
	defer w.RUnlock()
	return w.conf.lastBlockHash
}

// set, get TxTimes
func (w *Wallet) setTxTimes(val int) {
	w.Lock()
	defer w.Unlock()
	w.txTimes = val
}
func (w *Wallet) getTxTimes() int {
	w.RLock()
	defer w.RUnlock()
	return w.txTimes
}

// get lastBlockHash
func (w *Wallet) getLastBlockHash() string {
	w.RLock()
	defer w.RUnlock()
	return w.conf.lastBlockHash
}

// set, get key pool size
func (w *Wallet) setKeypoolsize(val int) {
	w.Lock()
	defer w.Unlock()
	w.keyPoolSize = val
}
func (w *Wallet) getKeyPoolSize() int {
	w.RLock()
	defer w.RUnlock()
	return w.keyPoolSize
}

// set, get rpc conf
func (w *Wallet) SetRpc(rpcUser, rpcPass, rpcServer string) {
	w.Lock()
	defer w.Unlock()
	w.conf.rpcUser, w.conf.rpcPass, w.conf.rpcServer = rpcUser, rpcPass, rpcServer
}
func (w *Wallet) getRpc() (string, string, string) {
	w.RLock()
	defer w.RUnlock()
	return w.conf.rpcUser, w.conf.rpcPass, w.conf.rpcServer
}

// Increment txTimes
func (w *Wallet) incrTxTimes() {
	txTimes, keyPoolSize := w.getTxTimes()+1, w.getKeyPoolSize()
	if txTimes > keyPoolSize {
		txTimes = 0
		if w.keyPoolRefillAndBackup() {
			w.incrTxTimes()
			return
		}
	}
	w.setTxTimes(txTimes)
}

// rpc Command
func (w *Wallet) rpcCommand(msg []byte) (btcjson.Reply, error) {
	rpcUser, rpcPass, rpcServer := w.getRpc()
	return btcjson.RpcCommand(rpcUser, rpcPass, rpcServer, msg)
}

// generate new address
// if return value is "" it means invalid address
func (w *Wallet) GenNewAddr(acct string) string {
	defer rec("Generate new address error: %q", w.logFunc)

	// incr tx times
	w.incrTxTimes()

	msg, err := btcjson.CreateMessage("getnewaddress", acct)
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	addr, ok := reply.Result.(string)
	if !ok {
		return w.GenNewAddr(acct)
	}

	return addr
}

// invalid address
// use the function to check if address is invalid before send coins to it
func (w *Wallet) IsValidAddress(address string) bool {
	defer rec("Validate address error: %q", w.logFunc)

	msg, err := btcjson.CreateMessage("validateaddress", address)
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	if v, ok := reply.Result.(btcjson.ValidateAddressResult); ok {
		return v.IsValid
	}
	return false
}

// send to address, TODO: need to test
func (w *Wallet) Sendto(address string, amount float64) string {
	defer rec("Send coin to address error: %q", w.logFunc)
	w.incrTxTimes()

	msg, err := btcjson.CreateMessage("sendtoaddress", address, amount-w.getTxFee())
	pan(err)

	w.walletPassPhrase(2)
	reply, err := w.rpcCommand(msg)
	pan(err)
	w.walletLock()

	v, ok := reply.Result.(string)
	if ok {
		return v
	}
	return ""
}

// walletpassphrase
func (w *Wallet) walletPassPhrase(sec int) bool {
	defer rec("WalletPassPhrase error: %q", w.logFunc)
	pass := w.getPassword()

	msg, err := btcjson.CreateMessage("walletpassphrase", pass, sec)
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	if reply.Error != nil {
		pan(reply.Error.Error())
	}

	return true
}

// wallet lock
func (w *Wallet) walletLock() {
	defer rec("WalletLock error: %q", w.logFunc)

	msg, err := btcjson.CreateMessage("walletlock")
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	if reply.Error != nil {
		pan(reply.Error.Error())
	}
}

// key pool refill & backup
func (w *Wallet) keyPoolRefillAndBackup() bool {
	defer rec("Keypool refill and backup error: %q", w.logFunc)
	w.walletPassPhrase(2)
	defer w.walletLock()

	// key pool refill
	msg, err := btcjson.CreateMessage("keypoolrefill")
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	if reply.Error != nil {
		pan(reply.Error.Error())
	}

	// backup
	backupDest := w.getBackupDest()
	msg, err = btcjson.CreateMessage("backupwallet", backupDest+fmt.Sprintf("%d.dat", time.Now().UnixNano()))
	pan(err)

	reply, err = w.rpcCommand(msg)
	pan(err)

	if reply.Error != nil {
		pan(reply.Error.Error())
	}

	w.logFunc("keypool refill and backup succeed at %s", backupDest)
	return true
}

// detect recv
func (w *Wallet) recvCoin() {
	defer rec("Detect receive error: %q", w.logFunc)
	detectInterval, lastBlockHash, minConfirmation := w.getDetectInterval(), w.getLastBlockHash(), w.getMinConf()
	defer time.AfterFunc(time.Duration(detectInterval)*time.Minute, func() {
		w.recvCoin()
	})

	msg, err := btcjson.CreateMessage("listsinceblock", lastBlockHash, minConfirmation)
	pan(err)

	reply, err := w.rpcCommand(msg)
	pan(err)

	if reply.Error != nil {
		pan(reply.Error.Error())
	}

	// get last block hash
	lastBlockHash, ok := reply.Result.(map[string]interface{})["lastblock"].(string)
	if !ok {
		pan(errors.New("can not get lastBlockHash"))
	}

	// get txSlice
	txSlice, ok := reply.Result.(map[string]interface{})["transactions"].([]interface{})
	if !ok {
		pan(errors.New("can not get transaction list"))
	}

	// make slice
	recvSlice := make([]Recv, len(txSlice))

	for i, v := range txSlice {
		if a, ok := v.(map[string]interface{}); ok {

			switch a["category"].(string) {

			case "receive":
				if a["confirmations"].(float64) >= float64(w.conf.minConfirmation) {
					txid := a["txid"].(string)
					acct := a["account"].(string)
					amount := a["amount"].(float64)
					recvSlice[i] = Recv{
						Acct:   acct,
						Txid:   txid,
						Amount: amount,
					}
				}

			default:
			}
		}
	}

	// insert into db
	// TODO: make sure it only inserts the tx which are not in db
	err = w.onRecv(recvSlice)
	pan(err)

	w.SetLastBlock(lastBlockHash)
}
